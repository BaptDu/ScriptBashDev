#!/bin/sh

make_changes_commit(){

  echo
  echo -n "Do you want to commit [y/n] ? : "
  read commit_reps

  if [ ${commit_reps} = "y" ]; then

    echo -e "Descriptive message :"
    read content

    git commit -m "$content"

  fi

  clear
}

make_changes_add(){

  echo
  echo -n "Do you want to save the files [y/n] ? : "
  read add_reps

  if [ ${add_reps} = "y" ]; then

    echo -n "Do you want to save all the files [y/n] ? : "
    read add

    if [ ${add} = "y" ]; then

      git add -A

      else

      while [ 1 ]; do
        echo "You are here: $(pwd)"
        echo "Select File or Folder [exit for return] : "
        read -e filename

        if [ ${filename} = "exit" ]; then
            break
        else
            git add ${filename}

        fi
      done
    fi
  fi

}

make_changes_reset(){

  echo
  echo -n "Are you sure you want to reset [y/n] ? : "
  read reset_reps

  if [ ${reset_reps} = "y" ]; then

    git reset --hard HEAD

  fi

}

make_changes_status(){

  clear

  git status --column -s -b

}