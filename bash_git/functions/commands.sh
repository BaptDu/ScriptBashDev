#!/bin/sh

commands_info(){
clear

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mconfig\e[0m"
echo -e "If you are in your first use,'config' allows you to set up your name and email when you push"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mclone\e[0m"
echo -e "Clone a repository into a new directory"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mclone_remote\e[0m"
echo -e "In progress ;)"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mstatus\e[0m"
echo -e "Show the working tree status"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34madd\e[0m"
echo -e "Add file contents to the index"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mcommit\e[0m"
echo -e "Record changes to the repository"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mpush\e[0m"
echo -e "Update remote refs along with associated objects"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mmerge\e[0m"
echo -e "Join two or more development histories together"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mreset\e[0m"
echo -e "Reset current HEAD to the specified state"

echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"
echo -e "\e[34mlog\e[0m"
echo -e "Show commit logs"
echo -e "\e[31m------------------------------------------------------------------------------------------------------\e[0m"


}